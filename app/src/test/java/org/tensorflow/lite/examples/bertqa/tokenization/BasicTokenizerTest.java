package org.tensorflow.lite.examples.bertqa.tokenization;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import static org.junit.Assert.*;

public class BasicTokenizerTest {
    BasicTokenizer basicTokenizer;

    @Before
   public void setUp() {  basicTokenizer = new BasicTokenizer(true);
    }

    @Test
   public void tokenize_lastWordWithQuestionMark_isTrue() {
         List<String> tokenized = basicTokenizer.tokenize("How are you?");
         assertEquals("?", tokenized.get(tokenized.size()-1));
    }


    @Test
    public void whitespaceTokenize_nOSpaceBetweenLastWordAndQmark_isTrue() {
        List<String> WStokenized = basicTokenizer.whitespaceTokenize("How are you?");
        assertEquals("How", WStokenized.get(0));
        assertEquals("you?", WStokenized.get(WStokenized.size()-1));
    }
}