/* Copyright 2019 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
package org.tensorflow.lite.examples.bertqa.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import org.tensorflow.lite.examples.bertqa.R;
import org.tensorflow.lite.examples.bertqa.ml.LoadDatasetClient;
import org.tensorflow.lite.examples.bertqa.ml.QaAnswer;
import org.tensorflow.lite.examples.bertqa.ml.QaClient;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/** Activity for doing Q&A on a specific dataset */
public class QaActivity extends AppCompatActivity {

  private static final String DATASET_POSITION_KEY = "DATASET_POSITION";
  private static final String TAG = "QaActivity";

  private TextInputEditText questionEditText;


  List<MessageChatModel> messageChatModelList =  new ArrayList<>();
    RecyclerView recyclerView;
    MessageChatAdapter adapter ;



  private boolean questionAnswered = false;
  private String content;
  private Handler handler;
  private QaClient qaClient;

  private String reponse;

  public static Intent newInstance(Context context, int datasetPosition) {
    Intent intent = new Intent(context, QaActivity.class);
    intent.putExtra(DATASET_POSITION_KEY, datasetPosition);
    return intent;
  }


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.v(TAG, "onCreate");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.tfe_qa_activity_qa);
 getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    // Get content of the selected dataset.
    int datasetPosition = getIntent().getIntExtra(DATASET_POSITION_KEY, -1);
    LoadDatasetClient datasetClient = new LoadDatasetClient(this);
    content = datasetClient.getContent(datasetPosition);
      // Show the dataset title.
      getSupportActionBar().setTitle(datasetClient.getTitles()[datasetPosition]);

   /*
    // Show the text content of the selected dataset.
      RecyclerView chatListView = findViewById(R.id.chat_list);
      QuestionAdapter chatAdapter =
              new QuestionAdapter(this, datasetClient.getQuestions(datasetPosition));
      chatAdapter.setOnQuestionSelectListener(question -> answerQuestion(question));
      chatListView.setAdapter(chatAdapter);
      LinearLayoutManager chatLayoutManager =
              new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
      chatListView.setLayoutManager(chatLayoutManager);*/

      ////////////////////////////////////////////////////////////////////////////


      recyclerView = (RecyclerView)findViewById(R.id.chat_list);
      LinearLayoutManager manager = new LinearLayoutManager(QaActivity.this, RecyclerView.VERTICAL, false);
      recyclerView.setLayoutManager(manager);



      DateTimeFormatter formatPattern = DateTimeFormatter.ofPattern("HH:mm");
      MessageChatModel model1 = new MessageChatModel(
              "Hello, ask me a question about "+ datasetClient.getTitles()[datasetPosition]+".",
              LocalDateTime.now().format(formatPattern),
              1
      );




      messageChatModelList.add(model1);



      recyclerView.smoothScrollToPosition(messageChatModelList.size());
      adapter = new MessageChatAdapter(messageChatModelList, QaActivity.this );
      recyclerView.setAdapter(adapter);

      ////////////////////////////////////////////////////////////////////////////////////////


    // Setup question suggestion list.
    RecyclerView questionSuggestionsView = findViewById(R.id.suggestion_list);
    QuestionAdapter adapter =
        new QuestionAdapter(this, datasetClient.getQuestions(datasetPosition));
    adapter.setOnQuestionSelectListener(question -> answerQuestion(question));
    questionSuggestionsView.setAdapter(adapter);
    LinearLayoutManager layoutManager =
        new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
    questionSuggestionsView.setLayoutManager(layoutManager);

    // Setup ask button.
    ImageButton askButton = findViewById(R.id.ask_button);
    askButton.setOnClickListener(
        view -> answerQuestion(questionEditText.getText().toString()));

    // Setup text edit where users can input their question.
    questionEditText = findViewById(R.id.question_edit_text);
    questionEditText.setOnFocusChangeListener(
        (view, hasFocus) -> {
          // If we already answer current question, clear the question so that user can input a new
          // one.
          if (hasFocus && questionAnswered) {
            questionEditText.setText(null);
          }
        });
    questionEditText.addTextChangedListener(
        new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

          @Override
          public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Only allow clicking Ask button if there is a question.
            boolean shouldAskButtonActive = !charSequence.toString().isEmpty();
            askButton.setClickable(shouldAskButtonActive);
            askButton.setImageResource(
                shouldAskButtonActive ? R.drawable.ic_ask_active : R.drawable.ic_ask_inactive);
          }

          @Override
          public void afterTextChanged(Editable editable) {}
        });
    questionEditText.setOnKeyListener(
        (v, keyCode, event) -> {
          if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
            answerQuestion(questionEditText.getText().toString());
          }
          return false;
        });

    // Setup QA client to and background thread to run inference.
    HandlerThread handlerThread = new HandlerThread("QAClient");
    handlerThread.start();
    handler = new Handler(handlerThread.getLooper());
    qaClient = new QaClient(this);
  }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

  @Override
  protected void onStart() {
    Log.v(TAG, "onStart");
    super.onStart();
    handler.post(
        () -> {
          qaClient.loadModel();
          qaClient.loadDictionary();
        });
  }

  @Override
  protected void onStop() {
    Log.v(TAG, "onStop");
    super.onStop();
    handler.post(() -> qaClient.unload());
  }

  private void answerQuestion(String question) {
    question = question.trim();
    if (question.isEmpty()) {
      questionEditText.setText(question);
      return;
    }

    // Append question mark '?' if not ended with '?'.
    // This aligns with question format that trains the model.
    if (!question.endsWith("?")) {
      question += '?';
    }
    final String questionToAsk = question;
    questionEditText.setText(questionToAsk);

    // Delete all pending tasks.
    handler.removeCallbacksAndMessages(null);

    // Hide keyboard and dismiss focus on text edit.
    InputMethodManager imm =
        (InputMethodManager) getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    View focusView = getCurrentFocus();
    if (focusView != null) {
      focusView.clearFocus();
    }


    questionAnswered = false;

    /*
    Snackbar runningSnackbar =
        Snackbar.make(contentTextView, "Thinking...", Integer.MAX_VALUE);
    runningSnackbar.show();
     */

    //We update the ui with the new question
messagePresenter(questionToAsk, 0);
questionEditText.setText("");

    // Run TF Lite model to get the answer.
/*
      handler.post(
        () -> {
          final List<QaAnswer> answers = qaClient.predict(questionToAsk, content);

          if (!answers.isEmpty()) {
            // Get the top answer
            QaAnswer topAnswer = answers.get(0);
            reponse = topAnswer.text;

          questionAnswered = true;



          }
          else
          {
              reponse="";
          }

        });*/


      // Show the answer.

final List<QaAnswer> answers = qaClient.predict(questionToAsk, content);

                  if (!answers.isEmpty()) {
                      // Get the top answer
                      QaAnswer topAnswer = answers.get(0);
                      messagePresenter(topAnswer.text, 1);

                      questionAnswered = true;

                  }
                  else
                  {
                      messagePresenter("Sorry, I am unable to answer that question", 1);
                  }










  }


  private void messagePresenter(String msg, int viewType)
  {

      DateTimeFormatter formatPattern = DateTimeFormatter.ofPattern("HH:mm");
      MessageChatModel model = new MessageChatModel(
              msg,
              LocalDateTime.now().format(formatPattern),
              viewType
      );

      messageChatModelList.add(model);
      recyclerView.smoothScrollToPosition(messageChatModelList.size());
      adapter.notifyDataSetChanged();
  }

}
