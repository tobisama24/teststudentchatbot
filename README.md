* The project is an android app and was built with android studio.
* I used JUnit to run the test.
* The test is on the BasicTokenizer class
* The test shows that the methods in that class are working as espected
* I could not run the text on the folloing classes because they need an object of the Context class to passed to their constructor. 
    Unfortunatly the Context class is available only when calling UI related methods like onCreate(). Therefore not available in the scope of JUnit 
    local unit testing.